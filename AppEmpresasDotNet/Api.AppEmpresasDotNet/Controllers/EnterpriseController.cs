﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.AppEmpresasDotNet.Entities;
using Domain.AppEmpresasDotNet.Exceptions;
using Infra.AppEmpresasDotNet.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Api.AppEmpresasDotNet.Controllers
{
    [Route("api/v1/enterprises")]
    [Authorize]
    public class EnterpriseController : Controller
    {
        private readonly IEnterpriseServices _enterpriseServices;

        public EnterpriseController(IEnterpriseServices enterpriseServices)
        {
            _enterpriseServices = enterpriseServices;
        }
        
        [HttpGet("{cdType:int}")]
        [ProducesResponseType(200, StatusCode = 200, Type = typeof(TbEnterprise))]        
        [ProducesResponseType(500, StatusCode = 500, Type = typeof(ValidationProblemDetails))]
        public IActionResult ConsultarPorCodigo([FromRoute] int cdType)
        {
            try
            {
                var enterprise = _enterpriseServices.ConsultarEmpresaPorTipo(cdType);

                return StatusCode(200, enterprise);
            }
            catch (NoContentException)
            {                
                return StatusCode(204);
            }
            catch (Exception)
            {
                var problemDetail = new ProblemDetails()
                {
                    Instance = HttpContext.Request.HttpContext.Request.Path,
                    Detail = "Ops...Aconteceu algo indesejado. Estamos trabalhando na solução."
                };

                return StatusCode(500, problemDetail);
            }
        }

        [HttpGet]
        [ProducesResponseType(200, StatusCode = 200, Type = typeof(TbEnterprise))]       
        [ProducesResponseType(500, StatusCode = 500, Type = typeof(ValidationProblemDetails))]
        public IActionResult ConsultarEmpresaPorTipoNome([FromQuery] int enterprise_types, string name)
        {
            try
            {
                var enterprise = _enterpriseServices.ConsultarEmpresaPorTipoNome(enterprise_types, name);

                return StatusCode(200, enterprise);
            }
            catch (NoContentException)
            {
                return StatusCode(204);
            }
            catch (Exception)
            {
                var problemDetail = new ProblemDetails()
                {
                    Instance = HttpContext.Request.HttpContext.Request.Path,
                    Detail = "Ops...Aconteceu algo indesejado. Estamos trabalhando na solução."
                };

                return StatusCode(500, problemDetail);
            }
        }

    }
}
