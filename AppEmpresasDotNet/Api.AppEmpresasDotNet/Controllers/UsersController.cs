﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.AppEmpresasDotNet.Exceptions;
using Domain.AppEmpresasDotNet.Models;
using Infra.AppEmpresasDotNet.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Api.AppEmpresasDotNet.Controllers
{
    [Route("api/v1/users")]
    public class UsersController : Controller
    {
        private readonly IAuthenticationServices _authenticationServices;
       
        public UsersController(IAuthenticationServices authenticationServices)
        {
            _authenticationServices = authenticationServices;       
        }

        [RequireHttps]
        [AllowAnonymous]
        [HttpPost("auth/sign_in")]
        [ProducesResponseType(200, StatusCode = 200, Type = typeof(AuthorizationAccessModel))]
        [ProducesResponseType(400, StatusCode = 400, Type = typeof(ValidationProblemDetails))]
        [ProducesResponseType(401, StatusCode = 400, Type = typeof(ValidationProblemDetails))]
        [ProducesResponseType(500, StatusCode = 500, Type = typeof(ValidationProblemDetails))]
        public IActionResult AutenticarUsers([FromBody] UsersAuthModel usersAuth)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var problemDetails = new ValidationProblemDetails(ModelState);

                    return StatusCode(400, problemDetails);
                }

                var authorizationAccess = _authenticationServices.AutenticarUsuario(usersAuth);

                return StatusCode(200, authorizationAccess);
            }
            catch (UnauthorizedException e)
            {
                var problemDetail = new ProblemDetails()
                {
                    Instance = HttpContext.Request.HttpContext.Request.Path,
                    Detail = e.Message
                };

                return StatusCode(401, problemDetail);
            }
            catch (Exception)
            {
                var problemDetail = new ProblemDetails()
                {
                    Instance = HttpContext.Request.HttpContext.Request.Path,
                    Detail = "Ops...Aconteceu algo indesejado. Estamos trabalhando na solução."
                };

                return StatusCode(500, problemDetail);
            }
        }
    }
}
