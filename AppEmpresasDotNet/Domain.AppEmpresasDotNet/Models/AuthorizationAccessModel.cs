﻿using System;
namespace Domain.AppEmpresasDotNet.Models
{
    public class AuthorizationAccessModel
    {
        public string AccessToken { get; set; }

        public string Email { get; set; }

        public string DateCreated { get; set; }

        public string DateExpiration { get; set; }
        
    }
}
