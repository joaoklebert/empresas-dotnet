﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Domain.AppEmpresasDotNet.Models
{
    public class UsersAuthModel
    {
        [Required]
        [EmailAddress(ErrorMessage = "Email invalido.")]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
