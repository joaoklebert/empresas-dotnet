﻿using System;
namespace Domain.AppEmpresasDotNet.Exceptions
{
    public class NoContentException : Exception
    {
        public NoContentException(string message):base(message)
        {
        }
    }
}
