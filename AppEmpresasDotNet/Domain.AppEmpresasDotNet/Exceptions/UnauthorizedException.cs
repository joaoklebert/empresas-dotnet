﻿using System;
namespace Domain.AppEmpresasDotNet.Exceptions
{
    public class UnauthorizedException : Exception
    {
        public UnauthorizedException(string message) : base (message)
        {
        }
    }
}
