﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.AppEmpresasDotNet.Entities
{
    [Table("TB_USER")]
    public class TbUser
    {
        [Key]
        [Column("DS_EMAIL")]
        [StringLength(100)]
        public string DsEmail { get; set; }
        [Required]
        [Column("DS_PASSWORD")]
        [StringLength(100)]
        public string DsPassword { get; set; }
    }
}
