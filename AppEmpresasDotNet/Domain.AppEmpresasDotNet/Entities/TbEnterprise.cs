﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.AppEmpresasDotNet.Entities
{
    [Table("TB_ENTERPRISE")]
    public class TbEnterprise
    {
        [Key]
        [Column("CD_TYPE")]
        [Display(Name = "enterprise_type_code")]
        public int CdType { get; set; }

        [Required]
        [Column("NM_ENTERPRISE")]
        [StringLength(50)]
        [Display(Name = "name")]
        public string NmEnterprise { get; set; }
    }
}
