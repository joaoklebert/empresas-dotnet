﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Domain.AppEmpresasDotNet.Entities;

namespace Infra.AppEmpresasDotNet.Context
{
    public class DbContextAppEmpresas : DbContext
    {        
        public DbContextAppEmpresas(DbContextOptions<DbContextAppEmpresas> options)
            : base(options)
        {
        }
        
        public virtual DbSet<TbEnterprise> TbEnterprise { get; set; }
        public virtual DbSet<TbUser> TbUser { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TbEnterprise>(entity =>
            {
                entity.Property(e => e.CdType).ValueGeneratedNever();

                entity.Property(e => e.NmEnterprise).IsUnicode(false);
            });

            modelBuilder.Entity<TbUser>(entity =>
            {
                entity.Property(e => e.DsEmail)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.DsPassword).IsUnicode(false);
            });
        }
    }
}
