﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Domain.AppEmpresasDotNet.Exceptions;
using Domain.AppEmpresasDotNet.Models;
using Infra.AppEmpresasDotNet.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;

namespace Infra.AppEmpresasDotNet.Implementation
{
    public class AuthenticationServices : IAuthenticationServices
    {
        private readonly IConfiguration _configuration;
        private readonly IUserServices _userServices;

        public AuthenticationServices(IConfiguration configuration, IUserServices userServices)
        {
            _configuration = configuration;
            _userServices = userServices;
        }
        
        public AuthorizationAccessModel AutenticarUsuario(UsersAuthModel usersAuth)
        {
            try
            {                
                if (!_userServices.ValidarUsuario(usersAuth))
                {
                    throw new UnauthorizedException("Email ou senha inválidos.");
                }

                var issue = _configuration["Jwt:Issuer"];

                var claims = new[] {
                    new Claim(JwtRegisteredClaimNames.Email, usersAuth.Email),
                    new Claim(JwtRegisteredClaimNames.Iss,issue)
                };
         
                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                var minutes = 60;

                var token = new JwtSecurityToken(issue,
                                                 usersAuth.Email,
                                                 claims,
                                                 expires: DateTime.Now.AddMinutes(minutes),
                                                 signingCredentials: creds);

                var tokenBuild = new JwtSecurityTokenHandler().WriteToken(token);

                DateTime dateCreated = DateTime.Now;
                DateTime dateExpiration = dateCreated + TimeSpan.FromMinutes(minutes);

                return new AuthorizationAccessModel
                {
                    DateCreated = dateCreated.ToString("yyyy-MM-dd HH:mm:ss"),
                    DateExpiration = dateExpiration.ToString("yyyy-MM-dd HH:mm:ss"),
                    Email = usersAuth.Email,
                    AccessToken = tokenBuild                    
                };
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
