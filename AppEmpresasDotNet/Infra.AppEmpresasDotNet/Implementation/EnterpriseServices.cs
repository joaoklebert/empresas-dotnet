﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.AppEmpresasDotNet.Entities;
using Domain.AppEmpresasDotNet.Exceptions;
using Infra.AppEmpresasDotNet.Context;
using Infra.AppEmpresasDotNet.Interfaces;

namespace Infra.AppEmpresasDotNet.Implementation
{
    public class EnterpriseServices : IEnterpriseServices
    {
        private readonly DbContextAppEmpresas _dbContextAppEmpresas;

        public EnterpriseServices(DbContextAppEmpresas dbContextAppEmpresas)
        {
            _dbContextAppEmpresas = dbContextAppEmpresas;
        }

        public TbEnterprise ConsultarEmpresaPorTipo(int codigoTipo)
        {
            var enterprise = _dbContextAppEmpresas.TbEnterprise.Where(x => x.CdType == codigoTipo).FirstOrDefault();

            if (enterprise == null)
            {
                throw new NoContentException("Empresa não encontrada.");
            }

            return enterprise;
        }

        public IList<TbEnterprise> ConsultarEmpresaPorTipoNome(int codigoTipo, string nomeEmpresa)
        {
            var enterprise = _dbContextAppEmpresas.TbEnterprise.Where(x => x.CdType == codigoTipo &&
                                                                           x.NmEnterprise.Contains(nomeEmpresa))
                                                                    .ToList();

            if (enterprise == null || enterprise.Count == 0)
            {
                throw new NoContentException("Empresa não encontrada.");
            }

            return enterprise;
        }
    }
}
