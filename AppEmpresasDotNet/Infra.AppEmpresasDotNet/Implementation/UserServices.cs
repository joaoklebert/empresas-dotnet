﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Domain.AppEmpresasDotNet.Entities;
using Domain.AppEmpresasDotNet.Models;
using Infra.AppEmpresasDotNet.Context;
using Infra.AppEmpresasDotNet.Interfaces;

namespace Infra.AppEmpresasDotNet.Implementation
{
    public class UserServices : IUserServices
    {
        private readonly DbContextAppEmpresas _dbContextAppEmpresas;

        public UserServices(DbContextAppEmpresas dbContextAppEmpresas)
        {
            _dbContextAppEmpresas = dbContextAppEmpresas;
        }

        public bool ValidarUsuario(UsersAuthModel usersAuthModel)
        {
            bool credenciaisValidas = false;

            var usuarioBase = _dbContextAppEmpresas.TbUser.Where(x => x.DsEmail == usersAuthModel.Email)
                                    .FirstOrDefault<TbUser>();

            credenciaisValidas = (usuarioBase != null &&
                                  usersAuthModel.Email == usuarioBase.DsEmail &&
                                  GetHash(usersAuthModel.Password) == usuarioBase.DsPassword);
            

            return credenciaisValidas;
        }

        private string GetHash(string input)
        {
            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            var sBuilder = new StringBuilder();

            using (SHA256 sha256Hash = SHA256.Create())
            {
                // Convert the input string to a byte array and compute the hash.
                byte[] data = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

                // Loop through each byte of the hashed data 
                // and format each one as a hexadecimal string.
                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }
    }
}
