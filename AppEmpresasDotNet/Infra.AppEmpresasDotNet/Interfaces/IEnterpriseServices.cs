﻿using System;
using System.Collections.Generic;
using Domain.AppEmpresasDotNet.Entities;

namespace Infra.AppEmpresasDotNet.Interfaces
{
    public interface IEnterpriseServices
    {
        TbEnterprise ConsultarEmpresaPorTipo(int codigoTipo);

        IList<TbEnterprise> ConsultarEmpresaPorTipoNome(int codigoTipo, string nomeEmpresa);
    }
}
