﻿using System;
using Domain.AppEmpresasDotNet.Entities;
using Domain.AppEmpresasDotNet.Models;

namespace Infra.AppEmpresasDotNet.Interfaces
{
    public interface IUserServices
    {
        bool ValidarUsuario(UsersAuthModel usersAuthModel);
    }
}
