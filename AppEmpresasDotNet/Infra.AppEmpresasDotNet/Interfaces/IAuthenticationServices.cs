﻿using System;
using System.Threading.Tasks;
using Domain.AppEmpresasDotNet.Models;

namespace Infra.AppEmpresasDotNet.Interfaces
{
    public interface IAuthenticationServices
    {
        AuthorizationAccessModel AutenticarUsuario(UsersAuthModel usersAuth);
    }
}
